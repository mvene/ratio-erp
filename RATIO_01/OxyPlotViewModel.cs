﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Series;

namespace RATIO_01
{
    public class OxyPlotViewModel1 : ObservableObject
    {



        private PlotModel modelP1;

        private ObservableCollection<PieSlice> _datos = new ObservableCollection<PieSlice>();

        public ObservableCollection<PieSlice> datos { get { return _datos; } set { _datos = value; RaisePropertyChanged("datos"); } }

        public OxyPlotViewModel1()
        {
            modelP1 = new PlotModel { Title = "Pie Sample1" };

            PieSeries seriesP1 = new PieSeries { StrokeThickness = 2.0, InsideLabelPosition = 0.8, AngleSpan = 360, StartAngle = 180};
            datos.Add(new PieSlice("Africa", 1030) { IsExploded = false, Fill = OxyColors.PaleVioletRed });
            datos.Add(new PieSlice("Americas", 929) { IsExploded = true });
            datos.Add(new PieSlice("Asia", 4157) { IsExploded = true });
            datos.Add(new PieSlice("Europe", 739) { IsExploded = true });
            datos.Add(new PieSlice("Oceania", 35) { IsExploded = true });

            seriesP1.Slices = datos;
            modelP1.Series.Add(seriesP1);
            //Modificacion
        }

        public PlotModel Model1
        {
            get { return modelP1; }
            set
            {
                modelP1 = value;
                RaisePropertyChanged("Model1");
            }
        }

    }
}

