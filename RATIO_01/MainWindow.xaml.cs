﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Data.OleDb;

using System.Data.Entity;
using System.ComponentModel;
using System.Collections.ObjectModel;

using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using OxyPlot;
using OxyPlot.Series;

namespace RATIO_01
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window


    {
        RATIOEntities context = new RATIOEntities();
        ChartValues<Int16> Valores = new ChartValues<Int16> {};
        ChartValues<OhlcPoint> Valores1 = new ChartValues<OhlcPoint> {};
        CollectionViewSource estadoProyectosViewSource;
        CollectionViewSource proyectosViewSource;
        Double Valor = 30;
        ObservableCollection<TodoItem> items = new ObservableCollection<TodoItem>();

        OxyPlotViewModel1 viewPlot = new OxyPlotViewModel1();
        OxyPlotViewModel1 viewPlot2 = new OxyPlotViewModel1();
        public MainWindow()
        {

            InitializeComponent();
            OxyPlot1.DataContext = viewPlot;
            OxyPlot2.DataContext = viewPlot2;
            estadoProyectosViewSource = ((CollectionViewSource)(FindResource("estadoProyectosViewSource")));
            proyectosViewSource = ((CollectionViewSource)(FindResource("proyectosViewSource")));
            OxyPlot1.InvalidatePlot(true);
            
           
            //pro.Texto = "dondefue231132";
            // DataContext = this;
            // Angular1.Value = Valor;
        }
     


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            context.EstadoProyectos.Load();
            context.Proyectos.Load();
            estadoProyectosViewSource.Source = context.EstadoProyectos.Local;
            proyectosViewSource.Source = context.Proyectos.Local;
          
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //           context.SaveChanges();
            ///   pro.Texto = "dondefu3231231231231231231231231231231231233213123123e3";
            ///   
          //  items.RemoveAt(1);
            //items.RemoveAt(0);

        }
        private void BtonAgregar(object sender, RoutedEventArgs e)
        {
            //           context.SaveChanges();
            ///   pro.Texto = "dondefu3231231231231231231231231231231231233213123123e3";
            ///   
            //  items.RemoveAt(1);
            //items.RemoveAt(0);
            viewPlot.datos.Add(new PieSlice("Marte", 1739) { IsExploded = true });
            OxyPlot1.InvalidatePlot(true);

           // OxyPlot2.InvalidatePlot(true);

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //pro.Texto = "dondefue2";
            OhlcPoint valor1 = new OhlcPoint(32, 35, 30, 33);
            Valores1.Add(valor1);
            Proyectos newProyecto = new Proyectos { ProyectoId = 2, EstadoProyectoID = 10, FechaFin = new DateTime(12, 04, 12), FechaInicio = new DateTime(12, 02, 23), MontoPresupuesto = 45000, MontoVenta = 60000, Nombre = "ProyectoNuevo" };
            ///  context.EstadoProyectos.Add(newEstadosProyectos);
            context.Proyectos.Add(newProyecto);
            generarNuevos();
        }

        private void ejemplo1(object sender, RoutedEventArgs e)
        {
            ///'  Double val = new Random();

            estadoProyectosViewSource.View.MoveCurrentToNext();
            if (estadoProyectosViewSource.View.IsCurrentAfterLast)
                estadoProyectosViewSource.View.MoveCurrentToFirst();

            var datosProyecto = estadoProyectosViewSource.View.CurrentItem as EstadoProyectos;

            Valor = datosProyecto.EstadoProyectoID;
            // DataContext = this;
            //Angular1.Value = Valor;
            MessageBox.Show(Valor.ToString());
            Valores.Add(32);
            OhlcPoint valor1 = new OhlcPoint(32, 35, 30, 31);
        }

        public void generarNuevos()
        {


            //items.Add(new TodoItem() { Title = "Complete this WPF tutorial", Completion = 45 });
            //items.Add(new TodoItem() { Title = "Learn C#", Completion = 80 });
            //items.Add(new TodoItem() { Title = "Wash the car", Completion = 0 });

          //  icTodoList.ItemsSource = ;
        }
    }




    

    
public class TodoItem
{
    public string Title { get; set; }
    public int Completion { get; set; }
}

}

