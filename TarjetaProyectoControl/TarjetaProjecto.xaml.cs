﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.CompilerServices;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;


namespace TarjetaProyectoControl
{
    /// <summary>
    /// Lógica de interacción para UserControl1.xaml
    /// </summary>
    public partial class TarjetaProjecto : UserControl, INotifyPropertyChanged
    {
        //   private string _texto;

        public event PropertyChangedEventHandler PropertyChanged;

        public static DependencyProperty TextoProperty = DependencyProperty.Register("Texto", typeof(string), typeof(TarjetaProjecto));
        public static DependencyProperty TextoBotonProperty = DependencyProperty.Register("TextoBoton", typeof(string), typeof(TarjetaProjecto));
        public static DependencyProperty EstadoProperty = DependencyProperty.Register("Estado", typeof(int), typeof(TarjetaProjecto));

        private double Valor { get; set; }
        public int Estado
        {
            set
            {
                //MessageBox.Show("weee");
                
               
                  
               


                if (value != (int)GetValue(TextoProperty))
                {
                    

                  //  SetValue(EstadoProperty, value);
                  //  SetValue(progessEstado.Foreground, Brushes.Blue);
                    
                    NotifyPropertyChanged();
                    
                }
            }
            get
            {
                return (int)GetValue(EstadoProperty);

            }
        }

        public string Texto{
            set
            {
                
                if (value != (string)GetValue(TextoProperty))
                {
                    SetValue(TextoProperty, value);
                    NotifyPropertyChanged();
                }
            }
            get {
                return (string)GetValue(TextoProperty);
                
            }
        }

        public string TextoBoton{
            set
            {
                if (value != (string)GetValue(TextoBotonProperty))
                {
                    SetValue(TextoBotonProperty, value);
                    NotifyPropertyChanged();
                }
            }
            get
            {
                return (string)GetValue(TextoBotonProperty);
                
            }
        }

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                
            }
        }
    

        public TarjetaProjecto()
        {
            InitializeComponent();
            //progessEstado.Foreground = Brushes.Red;
            ///LayoutRoot.DataContext = this;
            //this.DataContext = this;

        }

        private void Mensaje2(object sender, EventArgs e)
        {

            MessageBox.Show("Donde"+ this.TextoBoton);
        }

    }
}
